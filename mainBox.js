export default class MainBox {

    constructor( {timezone, country, region, city, regionName} ){
        this.timezone = timezone;
        this.country = country;
        this.region = region;
        this.city = city;
        this.regionName = regionName;
        this._DOMelements = {
            parentEl:       document.querySelector('.container'),
            timezoneEl:    document.createElement("p"),
            countryEl:      document.createElement("p"),
            regionEl:       document.createElement("p"),
            cityEl:         document.createElement("p"),
            regionNameEl:   document.createElement("p"),
        }
    }
    render(){
        const {timezone, country, region, city, regionName} = this;
        const {parentEl, timezoneEl, countryEl, regionEl, cityEl, regionNameEl} = this._DOMelements;

        timezoneEl.innerText = `Continent :  ${timezone}`;
        countryEl.innerText =   `Country :    ${country}`;
        regionEl.innerText =    `Region :     ${region}`;
        cityEl.innerText =      `City :       ${city}`;
        regionNameEl.innerText =`City region :   ${regionName}`;

        parentEl.append(regionNameEl, countryEl, regionEl, cityEl, regionNameEl);
    }
}