import MainBox from "./mainBox.js";

const ipButton = document.getElementById('btn');
const myIP = document.createElement('p');
myIP.className = "my-ip";
const container = document.querySelector('.container');
const ipURL = "https://api.ipify.org/?format=json";
const locationURL = "http://ip-api.com/json/";


async function getLocationData(ipAddress){
    return await fetch(locationURL)
        .then(res=> res.json());
}

async function getIp(){
    const myIpAddress = await fetch(ipURL).then(adr=> adr.json());
    myIP.innerText = `myIP:   ${myIpAddress.ip}`;
    container.append(myIP);

const location = await getLocationData(myIpAddress);
    console.log(location);
    const mainContainer = new MainBox(location);
    mainContainer.render();
}



ipButton.addEventListener("click", () => getIp() );
container.append(ipButton);
